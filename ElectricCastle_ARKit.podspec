
Pod::Spec.new do |s|
  #

  s.name         = "ElectricCastle_ARKit"
  s.version      = "0.0.1"
  s.summary      = "AR framework for the EC app"
  s.description  = <<-DESC
  Adds AR features to the EC app.
                   DESC

  s.homepage     = "https://felecan@bitbucket.org/"
  s.license      = { :type => "MIT", :file => "license" }
  s.author             = { "Bianca Felecan" => "bianca.felecan@gmail.com" }
  s.ios.deployment_target = "10.0"
  s.ios.vendored_frameworks = 'ElectricCastle_ARKit.framework'
  s.source       = { :git => "https://bitbucket.org/izvorrr/electriccastle_arkit.git" }
  s.exclude_files = "Classes/Exclude"



  s.swift_version = "4"

  s.resources = 'ElectricCastle_ARKit.framework/**/*.xcassets', 'ElectricCastle_ARKit.framework/**/*.xib', 'ElectricCastle_ARKit.framework/**/*.nib', 'ElectricCastle_ARKit.framework/**/*.scnassets', 'ElectricCastle_ARKit.framework/**/*.png'
  s.resource_bundles = {
    'ElectricCastle_ARKit' => ['ElectricCastle_ARKit.framework/**/*.xcassets', 'ElectricCastle_ARKit.framework/**/*.xib', 'ElectricCastle_ARKit.framework/**/*.nib', 'ElectricCastle_ARKit.framework/**/*.scnassets', 'ElectricCastle_ARKit.framework/**/*.png}', 'ElectricCastle_ARKit.framework/**/GoogleService-Info-AR.plist']
  }

   s.static_framework = true

   s.pod_target_xcconfig = {
    'SWIFT_VERSION' => '4',
    'OTHER_LDFLAGS' => '$(inherited) -ObjC'
  }

end

//
//  ElectricCastle_ARKit.h
//  ElectricCastle+ARKit
//
//  Created by Bianca Felecan on 4/28/18.
//  Copyright © 2018 Izvor. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ElectricCastle_ARKit.
FOUNDATION_EXPORT double ElectricCastle_ARKitVersionNumber;

//! Project version string for ElectricCastle_ARKit.
FOUNDATION_EXPORT const unsigned char ElectricCastle_ARKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ElectricCastle_ARKit/PublicHeader.h>

